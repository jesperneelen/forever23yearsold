import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import { renderRoutes } from './router';

Meteor.startup(() => {
	render(
		renderRoutes(),
		document.getElementById('app')
	);
});
