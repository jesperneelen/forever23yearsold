import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import MainLayout from '../ui/layouts/main';
import Authenticated from '../ui/containers/authenticated';
import LoginPage from '../ui/pages/login/index';
import ContactsPage from '../ui/pages/contacts/index';
import ContactDetailPage from '../ui/pages/contact-details/index';

export const renderRoutes = () => {
	return (
		<Router history={browserHistory}>
			<Route path="/">
				<IndexRoute component={LoginPage}/>
				<Route path="login" component={LoginPage} />

				<Route component={Authenticated}>
					<Route component={MainLayout}>
						<Route path="contacts" component={ContactsPage}>
							<Route path=":contactId" component={ContactDetailPage} />
						</Route>
					</Route>
				</Route>
			</Route>
		</Router>
	);
};
