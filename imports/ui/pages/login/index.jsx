import React from 'react';

export default class LoginPage extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<hgroup>
					<h1>Forever23YearsOld</h1>
					<h3>By Jesper Neelen</h3>
				</hgroup>
				<form>
					<div className="group">
						<input type="text"/>
						<span className="highlight"/>
						<span className="bar"/>
						<label>Name</label>
					</div>
					<div className="group">
						<input type="email"/>
						<span className="highlight"/>
						<span className="bar"/>
						<label>Email</label>
					</div>
					<button type="button" className="button buttonBlue">
						Login
						<div className="ripples buttonRipples">
							<span className="ripplesCircle"/>
						</div>
					</button>
				</form>
			</div>
	);
	}
	}
