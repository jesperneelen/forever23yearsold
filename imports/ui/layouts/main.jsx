import React from 'react';

export default class MainLayout extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				Dit is de main layout
				{this.props.children}
			</div>
		);
	}
}
